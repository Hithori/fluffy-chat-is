import 'package:fluffychat/external/helper/colors.dart';
import 'package:flutter/cupertino.dart';
import 'colors.dart';

class Counter extends StatelessWidget {
  int counter;
  bool flagTag;
  double width, height, blur, spread, opacity, textSize, padding;
  Color color;

  @override
  Widget build(BuildContext context) {
    return counter != 0 || counter == null
        ? Container(
        height: height,
        constraints: BoxConstraints(minWidth: width),
        margin: EdgeInsets.fromLTRB(4, 4, 0, 0),
        padding: EdgeInsets.only(top: 2),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(30),
          boxShadow: [
            BoxShadow(
              color: color.withOpacity(opacity),
              spreadRadius: spread,
              blurRadius: blur,
            )
          ],
        ),
        child: (counter == null)
            ? flagTag
            ? Text(
          '@',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'SFProText',
          ),
        )
            : Text(
          '',
        )
            : Text(
          '$counter',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'SFProText',
            fontSize: textSize,
            color: ISColors.white,
          ),
        ))
        : Container();
  }

  Counter(int size, [int counter, Color color]) {
    switch (size) {
    //small size
      case 0:
        width = 6;
        height = 6;
        blur = 1;
        spread = 1;
        opacity = 0.2;
        padding = 0;
        this.color = color ?? ISColors.blue;
        flagTag = false;
        break;

    //medium size
      case 1:
        width = 17;
        height = 17;
        blur = 8;
        spread = 1;
        opacity = 0.5;
        this.counter = counter;
        this.color = color ?? ISColors.blue;
        textSize = 11;
        padding = 4;
        flagTag = false;
        break;

    //large size
      case 2:
        width = 24;
        height = 24;
        blur = 8;
        spread = 2;
        opacity = 0.5;
        this.counter = counter;
        this.color = color ?? ISColors.blue;
        textSize = 15;
        padding = 0;
        flagTag = false;

        break;

      case 3:
        width = 24;
        height = 24;
        blur = 8;
        spread = 2;
        opacity = 0.5;
        this.color = color ?? ISColors.blue;
        textSize = 15;
        padding = 0;
        flagTag = true;
        break;
    }
  }
}