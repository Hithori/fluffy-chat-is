import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:matrix/matrix.dart';
import 'package:flutter/widgets.dart' as DefWidgets;
import 'colors.dart';

class Avatarka extends StatefulWidget {
  final Room room;
  Uri image;
  final isOnline;

  Avatarka(this.room, {this.isOnline}){
    image = room.avatar;
  }

  @override
  _AvatarkaState createState() => _AvatarkaState();
}

class _AvatarkaState extends State<Avatarka> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(/*vertical: 15,*/horizontal: 6),
      child: Stack(
        alignment: Alignment.bottomRight,
        children: [
          ClipRRect(
          borderRadius: BorderRadius.circular(30),
          child: CachedNetworkImage(
            imageUrl: widget.image.toString(),
            width: 56,
            height: 56,
            fit: BoxFit.cover,
            placeholder: (c, s) => Center(
                child: Text(
              ' ',
              style: TextStyle(fontSize: 18),
            )),
            errorWidget: (c, s, d) => Stack(
              children: [
                Container(
                    color: ISColors.blue.withAlpha(100),
                    child: Center(
                        child: Text(
                      widget.room.displayname[0],
                      style: TextStyle(
                          fontSize: 18,
                          color: ISColors.white,
                          decoration: TextDecoration.none),
                    ))),

              ],

            ),
          ),
        ),
          DefWidgets.Visibility(
              visible: widget.isOnline ? true : false,
              child: Container(
                margin: DefWidgets.EdgeInsets.only(bottom: 2, right: 6),
                width: 8,
                height: 8,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 8,
                        color: ISColors.blue.withOpacity(0.5)
                    )
                  ],
                  color: ISColors.green,
                  shape: BoxShape.circle,
                ),
              ))
        ],
      ),
    );
  }
}
