import 'package:flutter/cupertino.dart';
abstract class ISColors{
   static final green = const Color.fromRGBO(34, 178, 23, 1);
   static final blue = const Color.fromRGBO(0, 145, 255, 1);
   static final darkBlue = const Color.fromRGBO(45, 47, 182, 0.5);
   static final grey = const Color.fromRGBO(60, 60, 67, 0.6); ///
   static final transparentGrey = const Color.fromRGBO(60, 60, 67, 0.3);
   static final black = const Color.fromARGB(255, 0, 0, 0);
   static final white = const Color.fromARGB(255, 255, 255, 255);
   static final transparent = const Color.fromRGBO(0, 0, 0, 0);
}

abstract class DarkText {
   static final primary = Color.fromRGBO(0, 0, 0, 1); //#000, 100%
   static final secondary = Color.fromRGBO(60, 60, 67, 0.6); //#3C3C43, 60%
   static final tertiary = Color.fromRGBO(60, 60, 67, 0.3); //#3C3C43, 30%
   static final quatemary = const Color.fromRGBO(60, 60, 67, 0.18); //#3C3C43, 18%
}

abstract class Icons {
   static final inactive = Color.fromRGBO(153, 162, 173, 1);
   static final active = Color.fromRGBO(45, 47, 182, 1);
}