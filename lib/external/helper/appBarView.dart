import 'package:fluffychat/external/helper/decor.dart';
import 'package:fluffychat/external/search/cupertino_style_search.dart';
import 'package:fluffychat/external/search/search_logic.dart';
import 'package:fluffychat/pages/chat_list.dart';
import 'package:fluffychat/widgets/matrix.dart';
import 'package:flutter/cupertino.dart';
import 'package:matrix/matrix.dart';
// import 'package:flutter/material.dart';

import 'Avatarka.dart';
import 'colors.dart';
import 'is_nav_bar.dart';
import 'tab_controller_remake.dart';
import 'tabs_remake.dart';
import '../models/chat.dart';
import '../models/folder.dart';
import '../test_data/test_data.dart';
import '../theme/theme.dart';
import '../icons_chat.dart';
import 'dart:math' as math;
import 'dart:developer' as developer;

import '../view/chat/chatsView.dart';

/*
* Avatarka(room, isOnline: true,)
* */


class AppBarView extends StatefulWidget {
  List<SearchItem> Function(String text) search;
  final ChatListController controller;
  final List<Room> rooms;

  AppBarView(this.controller, this.rooms);

  @override
  _AppBarViewState createState() => _AppBarViewState(controller);
}

class _AppBarViewState extends State<AppBarView> with SingleTickerProviderStateMixin {
  final ChatListController controller;
  _AppBarViewState(this.controller);
  List<ScrollController> scrollController = <ScrollController>[];
  TabController tabController;
  FocusNode _focusNode;

  int page = 0;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: me.folders.length, vsync: this);
    tabController.addListener(() {
      developer.log(tabController.index.toString(), name: 'appBarView_tabController.index');
      developer.log(me.folders.elementAt(page).chatsID.toString(), name: 'appBarView_chatsID');
      setState(() {
        page = tabController.index;
      });
      scrollController.forEach((controller) => controller.jumpTo(controller.position.maxScrollExtent));
    });
    _focusNode = FocusNode();
    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        showPlatformSearch(
          context: context,
          delegate: CupertinoSearchDelegate(widget.search),
        );
      }
      _focusNode.unfocus();
    });
  }


  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final height = 100;
    final rooms = List<Room>.from(
        Matrix.of(context).client.rooms);
    rooms.removeWhere(
            (room) => room.lastEvent == null);
    var items = rooms.map((element)  {
      // print(element.toString());
      return SearchItem(element.displayname,
          Avatarka(element, isOnline: false));
    }).toList();
    List<SearchItem> search(String text) {
      return items
          .where((element) =>
          element.name.toLowerCase().contains(text.toLowerCase()))
          .toList();
    }
    return CupertinoPageScaffold(
      backgroundColor: ISColors.white,
      child: NestedScrollView(
        headerSliverBuilder: (context, flag){
          return [
            ISSliverNavigationBar(
              trailing: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  CupertinoButton(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 10),
                    onPressed: () {},
                    child: Icon(
                      Icons_Chat.call_plus_out_28,
                      color: ISColors.blue,
                      size: 28,
                    ),
                  ),
                  CupertinoButton(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 10),
                    onPressed: () {},
                    child: Icon(
                      Icons_Chat.pen_out_28,
                      color: ISColors.blue,
                      size: 28,
                    ),
                  ),
                ],
              ),
              previousPageTitle: '',
              border: Border.all(style: BorderStyle.none),
              strokeTitle: Text(
                'CHAT',
                maxLines: 1,
                style: TextStyle(
                  fontSize: 55,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'SFProDisplayBold',
                ),
              ),
              strokeColor: ISColors.darkBlue,
              largeTitle: Text(
                'Чат',
              ),
            ),
            SliverPersistentHeader(
              delegate: _SliverSearchBarDelegate(
                  search,
                  _focusNode,
                  collapsedHeight: height - 45.0,
                  expandedHeight: height - 45.0,
                  height: 0),
              pinned: false,
            ),
            SliverPersistentHeader(
              delegate: _SliverAppBarDelegate(
                  tabController: tabController,
                  collapsedHeight: height - 65.0,
                  expandedHeight: height - 65.0,
                  height: 38
              ),
              pinned: true,
            ),
          ];
        },
        body: Padding(
          padding: const EdgeInsets.only(top: 8),
          child: TabBarView(
                controller: tabController,
                children: me.folders.map((element) {
                  return ChatsList(
                    element,
                    controller: controller,
                    rooms: element.folderName == 'Непрочитано' ? widget.rooms.where((element) => element.notificationCount != 0).toList() : widget.rooms,
                  );
                }).toList(),
              ),
        )
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final double height;

  _SliverAppBarDelegate(
      {@required this.height,
      @required this.collapsedHeight,
      @required this.expandedHeight,
      @required this.tabController});

  final double expandedHeight;

  final double collapsedHeight;

  final TabController tabController;
  String activeKey;

  ScrollController menuController = ScrollController();
  Map<String, int> positions = {};

  @override
  double get minExtent => collapsedHeight;

  @override
  double get maxExtent => math.max(expandedHeight, minExtent);

  int countUnread(int folderID) {
    var counter = 0;
    if (folderID == 1) return 0;
    for (final i in me.folders) {
      if (i.folderID == folderID) {
        for (final j in chats) {
          if (i.chatsID.contains(j.chatID)) {
            if (j.unreadCount(me.userID) != 0) {
              counter++;
            }
          }
        }
        break;
      }
    }
    return counter;
  }

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      height: height,
      color:
          AppThemeSwitcherWidget.of(context).themeData.scaffoldBackgroundColor,
      child: Column(
        children: [
          Container(
            height: 35,
            child: ListView(
                // shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                children: [
                  TabBar(
                    isScrollable: true,
                    indicator: TabIndicator(
                      color: ISColors.blue,
                      radius: 2,
                    ),
                    labelColor: ISColors.black,
                    unselectedLabelColor: ISColors.grey,
                    indicatorPadding: EdgeInsets.symmetric(horizontal: 15),
                    controller: tabController,
                    tabs: me.folders.map((element) {
                      return SizedBox(
                        child: Tab(
                          text: element.folderName,
                          counter: countUnread(element.folderID),
                        ),
                      );
                    }).toList(),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      Icons_Chat.filter_out_24,
                      color: ISColors.grey,
                      size: 15,
                    ),
                  ),
                ]),
          ),
        ],
      ),
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return true; // activeKey != oldDelegate.activeKey; //expandedHeight != oldDelegate.expandedHeight || collapsedHeight != oldDelegate.collapsedHeight;
  }
}

class _SliverSearchBarDelegate extends SliverPersistentHeaderDelegate{
  Function(String) search;
  FocusNode _focusNode;
  final double expandedHeight;
  final double height;
  final double collapsedHeight;
  _SliverSearchBarDelegate(
      this.search,
      this._focusNode,
      {
        @required this.height,
        @required this.collapsedHeight,
        @required this.expandedHeight
      }
      );

  @override
  double get minExtent => collapsedHeight;

  @override
  double get maxExtent => math.max(expandedHeight, minExtent);



  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
      child: CupertinoTextField(
        decoration: BoxDecoration(color: ISColors.transparentGrey.withAlpha(20), borderRadius: BorderRadius.circular(10)),
        padding: EdgeInsets.all(10),
        prefix: Padding(
          padding: const EdgeInsets.only(left: 12.0),
          child: Icon(
            CupertinoIcons.search,
            color: ISColors.grey,
            size: 16,
          ),
        ),
        cursorColor: ISColors.blue,
        style: TextStyle(
            fontSize: 17,
            color: DarkText.secondary,
            fontFamily: 'SFProText',
            fontWeight: FontWeight.normal
        ),
        placeholder: 'Поиск',
        placeholderStyle: TextStyle(
            fontSize: 17,
            color: DarkText.secondary,
            fontFamily: 'SFProText',
            fontWeight: FontWeight.normal
        ),
        readOnly: true,
        onTap: () {
          showPlatformSearch(
            context: context,
            delegate: CupertinoSearchDelegate(search),
          );
        },
      ),
    );
  }

  @override
  bool shouldRebuild(_SliverSearchBarDelegate oldDelegate) {
    return true;
  }
}