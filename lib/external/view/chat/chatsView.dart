import 'package:fluffychat/pages/chat.dart';
import 'package:fluffychat/pages/chat_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:matrix/matrix.dart';
import '../../models/folder.dart';
import '../../models/chat.dart';
import '../../test_data/test_data.dart';
import 'chatRow.dart';

class ChatsList extends StatelessWidget {
  Folder tabChats;
  final List<Room> rooms;
  final ChatListController controller;

  ChatsList(this.tabChats, {this.controller, this.rooms});

  Widget build(BuildContext context) {
    return CustomScrollView(
        semanticChildCount: rooms.length, //общее количество элементов
        slivers: [
          SliverSafeArea(
              top: false,
              sliver: SliverList(
                  delegate: SliverChildBuilderDelegate((
                      context, index) =>
                    (index < rooms.length) //чтобы не выпадало за количество элементов (на всякий случай)
                      ? ChatRow(
                        rooms[index],
                        selected: controller.selectedRoomIds
                            .contains(rooms[index].id),
                        onTap: controller.selectMode == SelectMode.select
                            ? () => controller
                            .toggleSelection(rooms[index].id)
                            : null,
                        onLongPress: () => controller
                            .toggleSelection(rooms[index].id),
                        activeChat:
                          controller.activeChat == rooms[index].id,
                      )
                      : null
                  )
              )
          ),
        ]
    );
  }
}

/*Chat getChatByID(String id) {
  for (Chat i in chats) {
    if (i.chatID == id) {
      return i;
    }
  }
}*/
/*//final allChats;
    final unreadChats = chats.where((element) => element.messageList.last.readList.contains(user.userID) == false);
    //final privateChats = chats.where((element) => element.isStudy == false);
    final studyChats = chats.where((element) => element.isStudy == true);

    if (tabName == "Все") {
      return ListView(
          children: unreadChats.map((chat)  {
            return new ChatRow(chat, user);
          }).toList()
      );
    } else if (tabName == "Непрочитанные") {
      return ListView(
          children: unreadChats.map((chat)  {
            return new ChatRow(chat, user);
          }).toList()
      );
    } else if (tabName == "Учёба") {
      return ListView(
          children: studyChats.map((chat)  {
            return new ChatRow(chat, user);
          }).toList()
      );
    }*/
